package com.example.empdemo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppHealthCheck {

	@GetMapping("/hello")
	public String sayHello()
	{
		return "Hello";
	}
}
