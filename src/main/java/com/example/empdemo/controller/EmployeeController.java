package com.example.empdemo.controller;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.example.empdemo.entity.Employee;

@RepositoryRestResource(path="members")
public interface EmployeeController extends JpaRepository<Employee, Integer> {

}
